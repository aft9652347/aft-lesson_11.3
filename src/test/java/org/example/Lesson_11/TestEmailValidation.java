package org.example.Lesson_11;

import org.example.EmailValidation;
import org.example.IncorrectEmailException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestEmailValidation {
    //Создание объекта emailValidation для обращения к методам класса EmailValidation
    EmailValidation emailValidation = new EmailValidation();
    //Создание тестовых данных для позитивных тестов
    @DataProvider(name = "dataInvalidEmail")
    public Object[] dataProviderInvalidEmail() {
        return new Object[]{
                "sdaasd",
                "as@bas",
                "as.com"
        };
    }
    //Создание тестовых данных для негативных тестов
    @DataProvider(name = "dataValidEmail")
    public Object[] dataProviderValidEmail() {
        return new Object[]{
                "new@jods.ru",
                "sun@mail.net",
                "flay@kkj.com"
    };
}
    //Создание тестового метода с негативными кейсами для класса EmailValidation
    @Test(dataProvider = "dataInvalidEmail")
    public void TestInvalidEmailValidation(String validationEmail) {
        Assert.assertThrows(IncorrectEmailException.class, () -> emailValidation.getValidationEmail(validationEmail) );
    }

    ////Создание тестового метода с позитивными кейсами для класса EmailValidation
    @Test(dataProvider = "dataValidEmail")
    public void TestEmailValidation(String validationEmail) throws IncorrectEmailException {

       String actualResult = emailValidation.getValidationEmail(validationEmail);
       Assert.assertEquals(actualResult, "Проверено!");

    }

}
