package org.example.Lesson_11;

import org.example.ChetValidationNum;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestClassValidationNum {
    //Создание объекта для вызова методов класса
    ChetValidationNum chetValidationNum = new ChetValidationNum();
    //Делаем тесты параметризованными создаём набор данных
    @DataProvider(name = "TestDataToValidChetNum")
    public Object[][] dataProvider(){
        return new Object[][]{
                {2, true},
                {3, false},
                {0, true},
                {-1, false},
                {-8, true},
                {+3, false},
                {+4, true}
        };
    }

    //Позитивные кейсы негативные можно сделать что метод не принимает лишнего
    @Test(dataProvider = "TestDataToValidChetNum")
    public void testChetValidationNum(int validNum, boolean expectedResult){
        boolean actualResult = chetValidationNum.validationCheat(validNum);
        Assert.assertEquals(actualResult, expectedResult, "Негативный кейс пройден");
    }

}
