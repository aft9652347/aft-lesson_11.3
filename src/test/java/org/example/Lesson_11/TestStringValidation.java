package org.example.Lesson_11;

import org.example.StringValidation;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestStringValidation {
    //Создание объекта stringValidation клааса StringValidation для обращения к методам этого класса
    StringValidation stringValidation = new StringValidation();
    //Создание тестовых данных для параметризированного ТК
    @DataProvider(name = "dataStringValid")
    public Object[] dataProvider(){
        return new Object[]{
                Arrays.asList("порох","часовой","ударник", "вертолёт", "лимит", "машина", "патефон")
        };
    }
    //Тест для проверки валидации строк
    @Test(dataProvider = "dataStringValid")
    public void positivStringValid(List<String> filtrWordList){
        //Assert.assertEquals(stringValidation.newFiltrWordList(filtrWordList), "[порох, лимит, машина]");
        List<String> actualList = stringValidation.newFiltrWordList(filtrWordList);
        List<String> expectedList = Arrays.asList("порох", "лимит", "машина");
        Assert.assertEquals(actualList, expectedList);

    }
}
