package org.example;

    //Создаём класс для создания своего собственного исключения
public class IncorrectEmailException extends Exception {

    //создаём своё собственное исключение
    public IncorrectEmailException(String message){
        super(message);
    }

}
