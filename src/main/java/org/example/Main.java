package org.example;

public class Main {
    public static void main(String[] args) {
        //Отладим методы:
        //Валидация эмайл
        EmailValidation validEmail = new EmailValidation();
        //Валидация чётных чисел
        ChetValidationNum chetValidationNum = new ChetValidationNum();
        //Валидация строк
        StringValidation stringValidation = new StringValidation();

        //Блок обработки исключений
        try {
            validEmail.getValidationEmail("ds@aft.com");
        } catch (IncorrectEmailException e) {
            throw new RuntimeException(e);
        }

        boolean chetNum = chetValidationNum.validationCheat(3);
        System.out.println(chetNum);


        //System.out.println(stringValidation.wordList);
        System.out.println(stringValidation.newFiltrWordList(stringValidation.wordList));

    }
}